# 🎲 Play with the Gitpod CLI 

This project is a fun one to test and play with the [Gitpod CLI](https://www.gitpod.io/docs/references/gitpod-cli).

🚀 When you create a branch, a Gitpod workspace is created to allows you to work on the branch with Gitpod. This workspace is available is a [**GitLab environment**](https://docs.gitlab.com/ee/ci/environments/).

![GitLab Environment](./docs/env.png)

❌ When you stop this environment, your Gitpod workspace will be automatically removed.


## Configuration 

You can open this project with Gitpod after creating this variable : `GITPOD_TOKEN` [variable](https://gitpod.io/user/variables) in your Gitpod profile. You can create this variable from a [Gitpod access token](https://gitpod.io/user/tokens) generate in your profile too. This allows you to have a token that will expire on a choosen date.


[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/jeanphi-baconnais-experiments/playing-with-gitpod-cli/-/tree/main/)

_By Jean-Phi_